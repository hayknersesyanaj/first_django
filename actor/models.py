from django.db import models
from django.urls import reverse
from helpers.choices import AwardsChoices
from movie.models import Movie


class Actor(models.Model):

    first_name = models.CharField(max_length=155)
    last_name = models.CharField(max_length=155)
    age = models.PositiveIntegerField()
    awards = models.IntegerField(choices=AwardsChoices.choices,
                                 null=True, blank=True)
    image = models.ImageField(blank=True, null=True, upload_to="actor/images")
    movies = models.ManyToManyField(Movie, related_name="actor")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def get_absolute_url(self):
        return reverse("actor:detail", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.first_name} {self.last_name}"
