from django import forms

from actor.models import Actor


class ActorForm(forms.ModelForm):

    class Meta:
        model = Actor
        fields = ("first_name", "last_name",
                  "age", "awards", "image", "movies")
