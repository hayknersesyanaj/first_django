from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.shortcuts import render, get_object_or_404, redirect

from actor.forms import ActorForm
from actor.models import Actor


def actor_list(request):
    actors = Actor.objects.order_by("-id")
    paginator = Paginator(actors, 5)
    page_number = request.GET.get("page")
    page_obj = paginator.get_page(page_number)
    return render(request, "actor/list_actor.html",
                  context={"page_obj": page_obj})


def actor_detail(request, pk):
    actor = get_object_or_404(Actor, pk=pk)
    return render(request, "actor/detail_actor.html",
                  {"actor": actor})


@login_required
def create_actor(request):
    form = ActorForm()
    if request.method == "POST":
        form = ActorForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "successfully creating actor instance")
            return redirect("home")
    return render(request, "actor/create_actor.html", {"form": form})


@login_required
def update_actor(request, pk):
    actor = get_object_or_404(Actor, pk=pk)
    form = ActorForm(instance=actor)
    if request.method == "POST":
        form = ActorForm(request.POST, request.FILES, instance=actor)
        if form.is_valid():
            form.save()
            messages.error(request, "Actor object updated successfully")
            messages.success(request, "Just testing both cases")
            return redirect("actor:list")
    return render(request, "actor/update_actor.html", {"form": form})


@login_required
def delete_actor(request, pk):
    actor = get_object_or_404(Actor, pk=pk)
    if request.method == "POST":
        actor.delete()
        messages.info(request, "Actor deleted successfully")
        return redirect("actor:list")
    return render(request, "actor/delete_actor.html", {"actor": actor})