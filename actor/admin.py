from django.contrib import admin
from django.utils.html import format_html

from actor.models import Actor


class ActorAdmin(admin.ModelAdmin):
    list_display = ("first_name", "last_name", "age", "awards")
    list_display_links = ("first_name",)
    search_fields = ("first_name", "last_name")
    list_filter = ("created_at", "updated_at", "awards")
    list_editable = ("age", "last_name")
    list_per_page = 2
    readonly_fields = ("created_at", "updated_at", "actual_image")
    fieldsets = (
        (None, {"fields": ("first_name", "last_name")}),
        ("Personal Info", {"fields": (("age", "awards"), ("image", "actual_image"), "movies", "created_at", "updated_at")})
    )

    def get_search_fields(self, request):
        if request.user.is_superuser:
            return ("first_name",)
        else:
            return "first_name", "last_name"

    def actual_image(self, obj):
        return format_html("<img src='{}' alt='No Image'>", obj.image.url)





admin.site.register(Actor, ActorAdmin)
