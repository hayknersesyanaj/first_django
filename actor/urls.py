from django.urls import path
from actor import views

app_name = "actor"
urlpatterns = [
    path("list/", views.actor_list, name="list"),
    path("detail/<int:pk>/", views.actor_detail, name="detail"),
    path("create-actor/", views.create_actor, name="create"),
    path("update-actor/<int:pk>/", views.update_actor, name="update"),
    path("delete-actor/<int:pk>/", views.delete_actor, name="delete"),
]
