from django.contrib import messages
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, ListView, DetailView, UpdateView, DeleteView
from django.views.generic.base import TemplateResponseMixin, ContextMixin, View
from django.views.generic.edit import FormMixin

from users.forms import RegisterForm, LoginForm, DeleteForm
from users.mixins import OwnUserMixin


def register(request):
    form = RegisterForm()
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            instance = form.save()
            instance.profile.phone_number = form.cleaned_data["phone_number"]
            instance.profile.country = form.cleaned_data["country"]
            instance.profile.save()
            return redirect("home")
    return render(request, "user/registration.html",
                  {"form": form})


def login_user(request):
    form = LoginForm()
    next_url = request.GET.get("next")
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            user = get_object_or_404(User, username=form.cleaned_data["username"])
            login(request, user)
            if next_url:
                return redirect(next_url)
            return redirect("home")
    return render(request, "user/login.html", {"form": form})


def logout_user(request):
    if request.user.is_authenticated:
        logout(request)
    return redirect("home")


class ProfileListView(LoginRequiredMixin, ListView):
    queryset = User.objects.filter(is_superuser=False)
    template_name = "user/user_list.html"
    paginate_by = 2


class ProfileView(FormMixin, LoginRequiredMixin, DetailView):
    template_name = "user/profile.html"
    model = User
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "Profile"
        return context

    def get_form(self, form_class=None):
        return DeleteForm()

    def get_success_url(self):
        messages.info(self.request, "YOur account has been deleted")
        return reverse_lazy("home")

    def form_valid(self, form):
        self.object.delete()
        return super().form_valid(form)


class UpdateProfileView(OwnUserMixin, LoginRequiredMixin, UpdateView):
    model = User
    template_name = "user/profile_update.html"
    fields = ("first_name", "last_name", "email", "username")
    pk_url_kwarg = 'id'

    def get_success_url(self):
        messages.success(self.request, "Profile instance saved successfully")
        return reverse_lazy("user:profile", args=(self.object.pk,))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = "UPDATE|PROFILE"
        return context


class DeleteUserView(OwnUserMixin, LoginRequiredMixin, DeleteView):
    model = User
    template_name = "user/delete_account.html"
    pk_url_kwarg = 'id'

    def get_success_url(self):
        messages.warning(self.request, "Your Account has been deleted")
        return reverse_lazy("home")

