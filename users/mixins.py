from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from helpers.decorators import own_user_decorator


class OwnUserMixin:

    @method_decorator(own_user_decorator)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
