from django.urls import path
from users import views
from users.views import DeleteUserView

app_name = "users"
urlpatterns = [
    path("register/", views.register, name="register"),
    path("login/", views.login_user, name="login_user"),
    path("log-out/", views.logout_user, name="log_out_user"),
    path("profiles/list", views.ProfileListView.as_view(), name="profile-list"),
    path("profile/<int:id>/", views.ProfileView.as_view(), name="profile"),
    path("profile/update/<int:id>", views.UpdateProfileView.as_view(), name="update_profile"),
    path("profile/delete/<int:id>/", DeleteUserView.as_view(), name="user_delete"),
]
