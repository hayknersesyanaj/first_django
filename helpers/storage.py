
def upload_movie_image(instance, filename):
    return f"movies/{instance.title}/{filename}"
