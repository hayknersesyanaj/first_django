from django.core.validators import ValidationError


def validate_score(value: float):
    if 0 <= value <= 10:
        return value
    raise ValidationError("The score must be in 0-10 range")