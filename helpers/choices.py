from django.db import models


class GenreChoices(models.TextChoices):
    action = "action", "action"
    drama = "drama", "drama"
    fantasy = "fantasy", "fantasy"
    adventure = "adventure", "adventure"
    detective = "detective", "detective"


class AwardsChoices(models.IntegerChoices):
    oscar = 0, "oscar"
    voske_ciran = 1, "voske ciran"

