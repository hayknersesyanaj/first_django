from functools import wraps

from django.contrib import messages
from django.shortcuts import redirect


def own_user_decorator(func: callable):
    @wraps(func)
    def wrapper(request, *args, **kwargs):
        if request.user.id != kwargs.get("id"):
            messages.error(request, "You don't have permission to do this action")
            return redirect("home")
        return func(*args, **kwargs)
    return wrapper
