from django import forms

from helpers.choices import GenreChoices
from movie.models import Category, Movie


class MovieForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["description"].widget = forms.Textarea(attrs={"placeholder": "new placeholder"})

    class Meta:
        model = Movie
        fields = ("title", "description", "year", "genre",
                  "score", "total_min", "category", "image")
        help_texts = {"score": "ensure to pass the number between 0 and 10"}
        error_messages = {
            "score": {"required": "MAAAAAke sure to have a valid score"}
        }

    def clean_score(self):
        score = self.cleaned_data.get("score")
        if score > 10 or score < 0:
            raise forms.ValidationError("Score must be in range of 0-10")
        return score

    def save(self, commit=True, *args, **kwargs):
        movie_instance = super().save(commit=False)
        if movie_instance.year > 2024:
            movie_instance.year = 2023
        return movie_instance.save()


    # title = forms.CharField(max_length=155)
    # description = forms.CharField(widget=forms.Textarea())
    # year = forms.IntegerField()
    # genre = forms.CharField(widget=forms.RadioSelect(choices=GenreChoices.choices))
    # score = forms.FloatField()
    # total_min = forms.FloatField()
    # category = forms.ModelChoiceField(queryset=Category.objects.all())
    # image = forms.ImageField(required=False)
    #

    #
    # def clean(self):
    #     cleaned_dat = self.cleaned_data
    #     score = cleaned_dat.get("score")
    #     total_min = cleaned_dat.get("total_min")
    #     if score > total_min:
    #         raise forms.ValidationError("The score must be less than total min")
    #     return cleaned_dat


