from django.db import models

from helpers.choices import GenreChoices
from helpers.storage import upload_movie_image
from helpers.validators import validate_score


class Category(models.Model):
    category_name = models.CharField(max_length=155)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.category_name

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"


class Movie(models.Model):
    title = models.CharField(max_length=155)
    description = models.TextField()
    year = models.IntegerField()
    genre = models.CharField(max_length=155, choices=GenreChoices.choices,
                             default="action")
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="movie")
    score = models.FloatField(validators=[validate_score])
    total_min = models.FloatField(null=True, blank=True)
    image = models.ImageField(upload_to=upload_movie_image, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Kino"
        verbose_name_plural = "Kinoner"

    def __str__(self):
        return f"MOVIE {self.title}"

    def save(self, *args, **kwargs):
        self.description = self.description.replace(" ", "-->")
        super().save(*args, **kwargs)

