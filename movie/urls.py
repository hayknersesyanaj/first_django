from django.urls import path
from movie import views


urlpatterns = [
    path("", views.home, name="home"),
    path("movie-detail/<int:pk>/", views.movie_detail, name="movie_detail"),
    path("movie-by-category/", views.movie_by_categories, name="shnorhavor_amanor"),
    path("about-us/", views.about_us, name="about_us"),
    path("add-movie/", views.add_movie, name="add_movie"),
]
