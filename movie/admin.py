from django.contrib import admin
from django.utils.html import format_html

from movie.models import Movie, Category


class MovieAdmin(admin.ModelAdmin):
    list_display = ("title", "year", "genre", "score", "movie_image")
    list_per_page = 2
    list_filter = ("genre", "category__category_name")
    search_fields = ("title", "year")

    def movie_image(self, obj):
        if obj.image:
            return format_html("<img src='{}' style='width:80px;' alt='No Movie Image'>",
                               obj.image.url)
        return "NO Image"

    def get_search_results(self, request, queryset, search_term):
        if search_term.isdigit():
            return queryset.filter(year__gte=search_term), False
        return super().get_search_results(request, queryset, search_term)


class MovieInline(admin.StackedInline):
    model = Movie
    extra = 0


class CategoryAdmin(admin.ModelAdmin):
    list_display = ("category_name", "description")
    inlines = [MovieInline]


admin.site.register(Movie, MovieAdmin)
admin.site.register(Category, CategoryAdmin)
