from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.core.paginator import Paginator
from django.db.models import Q

from movie.forms import MovieForm
from movie.models import Movie, Category


def home(request):
    search = request.GET.get("search_data")
    if search and len(search) >= 3:

        all_movies = Movie.objects.filter(
            Q(title__icontains=search) | Q(description__icontains=search)
        )
    else:
        all_movies = Movie.objects.select_related("category").all()
    paginator = Paginator(all_movies, 2)
    page_number = request.GET.get("page")
    page_obj = paginator.get_page(page_number)
    all_categories = Category.objects.all()
    return render(
        request, "movie/home.html", {"page_obj": page_obj, "categories": all_categories}
    )


def movie_detail(request, pk):
    movie = get_object_or_404(Movie, pk=pk)
    all_categories = Category.objects.order_by("-pk")
    ctx = {"movie": movie, "categories": all_categories}
    return render(request, "movie/movie_detail.html", ctx)


def movie_by_categories(request):
    all_categories = Category.objects.all()
    category = request.GET.get("category")
    movies = Movie.objects.filter(category__category_name=category).all()
    ctx = {"movies": movies, "categories": all_categories}
    return render(request, "movie/movie_by_category.html", ctx)


@login_required
def add_movie(request):
    form = MovieForm(initial={"year": 2010, "title": "HHH"})
    if request.method == "POST":
        form = MovieForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect("home")
    return render(request, "movie/add_movie.html", {"form": form})


def about_us(request):
    all_categories = Category.objects.all()
    return render(request, "core/about_us.html", {"categories": all_categories})
